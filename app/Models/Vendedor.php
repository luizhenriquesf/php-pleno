<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vendedor extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vendedores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nome', 'email'];

    /**
     * A Vendedor may have multiple vendas.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vendas(){
        return $this->hasMany(Venda::class);
    }

    public function somaVendas(){
        return $this->hasMany(Venda::class)
            ->selectRaw('vendas.vendedor_id, SUM(vendas.comissao) as comissao')
            ->groupBy('vendas.vendedor_id');
    }
}
