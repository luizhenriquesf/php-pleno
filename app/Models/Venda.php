<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Venda extends Model
{
	    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['vendedor_id', 'valor' ,'comissao'];

	/**
     * A Venda can have a Vendedor.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
	public function vendedor()
    {
        return $this->hasOne(Vendedor::class);
    }

}
