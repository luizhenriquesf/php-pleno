<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use App\Repositories\VendaRepository;
use App\Http\Requests\VendaStoreRequest;

class VendaController extends Controller
{

    const cacheTime = 5; //5 minutes

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Repositories\VendaRepository $venda
     * @return void
     */
    public function index(VendaRepository $venda)
    {
        return Cache::remember('getVendas', self::cacheTime, function() use ($venda){
            return $venda->getVendas();
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Repositories\VendaRepository $venda
     * @param  \App\Http\Requests\VendaStoreRequest $request
     * @return void
     */
    public function store(VendaRepository $venda, VendaStoreRequest $request)
    {

        $fields = [
            'vendedor_id'   => $request->id,
            'valor'         => $request->valor,
            'comissao'      => (($request->valor / 100) * 6.5),
        ];

        $stored = $venda->create($fields);

        Cache::forget('getVendas');

        $result = $venda->getVendas($stored->vendedor_id);

        return response()->json($result, 201);
    }
}
