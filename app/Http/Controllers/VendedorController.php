<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use App\Repositories\VendedorRepository;
use App\Http\Requests\VendedorStoreRequest;

class VendedorController extends Controller
{

    const cacheTime = 5; //5 minutes

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Repositories\VendedorRepository $vendedor
     * @return void
     */
    public function index(VendedorRepository $vendedor)
    {
        return Cache::remember('listarVendedores', self::cacheTime, function() use ($vendedor){
            return $vendedor->listarVendedoresComComissao();
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Repositories\VendedorRepository $vendedor
     * @param  \App\Http\Requests\VendedorStoreRequest $request
     * @return void
     */
    public function store(VendedorRepository $vendedor, VendedorStoreRequest $request)
    {
        $stored = $vendedor->create($request->all());

        Cache::forget('listarVendedores');

        $result = collect($stored->toArray())->only(['id', 'nome', 'email'])->all();

        return response()->json($result, 201);
    }
}
