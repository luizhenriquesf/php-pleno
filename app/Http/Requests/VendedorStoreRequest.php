<?php

namespace App\Http\Requests;

class VendedorStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'  => "required|min:3|max:255",
            'email' => "required|email|unique:vendedores,email|max:255",
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nome.required' => 'O campo "Nome" é obrigatório.',
            'nome.min'      => 'O campo "Nome" deve conter no minimo :min caracteres.',
            'nome.max'      => 'O campo "Nome" deve conter no máximo :max caracteres.',

            'email.required'    => 'O campo "E-mail" é obrigatório.',
            'email.email'       => 'O campo "E-mail" deve ser um E-mail válido.',
            'email.unique'      => 'O campo "E-mail" deve único.',
            'email.max'         => 'O campo "E-mail" deve conter no máximo :max caracteres.',
        ];
    }
}
