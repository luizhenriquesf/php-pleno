<?php

namespace App\Http\Requests;


class VendaStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'    => "required|exists:vendedores",
            'valor' => "required|regex:/^\d+(\.\d{1,2})?$/|max:15",
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required'   => 'O campo "ID do Vendedor" é obrigatório.',
            'id.exists'     => 'O campo "ID do Vendedor" deve ser de um Vendedor cadastrado',

            'valor.required'=> 'O campo "Valor" é obrigatório.',
            'valor.regex'   => 'O campo "Valor" deve ser numérico e/ou decimal. Ex.: 1000 ou 1000.00',
            'valor.max'     => 'O campo "Valor" deve conter no máximo :max caracteres.',
        ];
    }
}
