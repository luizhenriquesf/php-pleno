<?php

namespace App\Repositories;

use App\User;

class UserRepository extends BaseRepository
{
	/**
     * @var Model
     */
    protected $model;

    /**
     * Create a new Repository instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }
}