<?php

namespace App\Repositories;

use App\Repositories\Traits\Relationable;
use App\Repositories\Traits\Sortable;

abstract class BaseRepository
{
    use Sortable, Relationable;

    /**
     * @var Model
     */
    protected $model;

    /**
     * Return all results of the given model from the database
     *
     * @param array $relationships
     * @param array $columns
     * @return mixed
     */
    public function all($relationships = [], $columns = ['*'])
    {
        $model = $this->model;
        $model = $this->relationships($model, $relationships);

        return $this->orderBy($model, $this->sortBy, $this->sortOrder)->get($columns);
    }

    /**
     * Return paginated results of the given model from the database
     *
     * @param $perPage
     * @param string $orderBy
     * @param string $direction
     * @param array $relationships
     * @return mixed
     */
    public function paginate($perPage = 15, $relationships = [])
    {
        $model = $this->model;
        $model = $this->relationships($model, $relationships);

        return $this->orderBy($model, $this->sortBy, $this->sortOrder)->paginate($perPage);
    }

    /**
     * Return a model by ID from the database. If relationships are provided,
     * eager load those relationships.
     *
     * @param $id
     * @param array $relationships
     * @return mixed
     */
    public function find($id, $relationships = [])
    {
        $model = $this->model;
        $model = $this->relationships($model, $relationships);
        $model = $model->findOrFail($id);

        return $model->first();
    }

    /**
     * Return a model by columns from the database. If relationships are provided,
     * eager load those relationships.
     *
     * @param $column
     * @param $value
     * @param array $relationships
     * @return mixed
     */
    public function findBy($column, $value, $relationships = [])
    {
        $model = $this->model;
        $model = $this->relationships($model, $relationships);
        $model = $model->where($column, $value);
        
        return $model->get();
    }

    public function create($input)
    {
        $model = $this->model;
        $model->fill($input);
        $model->save();

        return $model;
    }

    public function destroy($id)
    {
        return $this->model->find($id)->delete();
    }

    public function update($id, array $input)
    {
        $model = $this->model->find($id);
        $model->fill($input);
        $model->save();

        return $model;
    }

    /**
     * Load a model's relationships
     *
     * @param $model
     * @param array $relationships
     * @return mixed
     */
    private function relationships($model, $relationships = [])
    {
        return $model->with($relationships);
    }

    /**
     * Call Eloquent's order by method on the current model
     *
     * @param $model
     * @param $column
     * @param $direction
     * @return mixed
     */
    private function orderBy($model, $column, $direction)
    {
        return $model->orderBy($column, $direction);
    }
}