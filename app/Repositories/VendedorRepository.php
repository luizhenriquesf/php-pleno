<?php

namespace App\Repositories;

use DB;
use App\Models\Vendedor;

class VendedorRepository extends BaseRepository
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * Create a new Repository instance.
     *
     * @return void
     */
    public function __construct(Vendedor $vendedor)
    {
        $this->model = $vendedor;
    }

    /**
     * Create a new Repository instance.
     *
     * @return 
     */
    public function listarVendedoresComComissao(){
        return $this->model->select(['id','nome','email'])->with(['somaVendas'])->get();
    }
}