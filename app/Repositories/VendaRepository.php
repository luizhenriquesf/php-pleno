<?php

namespace App\Repositories;

use App\Models\Venda;

class VendaRepository extends BaseRepository
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * Create a new Repository instance.
     *
     * @return void
     */
    public function __construct(Venda $venda)
    {
        $this->model = $venda;
    }

    public function getVendas($idVendedor = null){
        $query = $this->model->select(['vendas.id','nome', 'email', 'valor' ,'comissao' ,'vendas.created_at as data_venda']);
        
        $query->leftjoin('vendedores as u', 'u.id', '=', 'vendas.vendedor_id');
        
        if($idVendedor){
            $query = $query->where('u.id', $idVendedor);
        }

        return $query->get();
    }
}