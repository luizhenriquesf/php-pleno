<?php

namespace App\Repositories\Traits;

trait Sortable
{
    /**
     * @var sortBy
     */
    public $sortBy = 'created_at';

    /**
     * @var sortOrder
     */
    public $sortOrder = 'asc';

    public function setSortBy($sortBy = 'created_at')
    {
        $this->sortBy = $sortBy;
    }

    public function setSortOrder($sortOrder = 'desc')
    {
        $this->sortOrder = $sortOrder;
    }
}